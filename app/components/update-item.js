import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    updateItem1(item) {

      var params = {
        description: item.get('description'),
        cost: item.get('cost'),
        details: item.get('details'),
        image: item.get('image')
      };
      this.sendAction('updateItem2', item, params);
    }
  },
});
